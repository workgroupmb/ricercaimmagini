import csv
from db_utils import create_db_connection


with open('final_correlations.csv', mode='w') as csv_file:
    wrt = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)

    wrt.writerow(['external_code', 'corr_mean_hist_cos','corr_mean_hist_euc','corr_mean_net_cos','corr_mean_net_euc','corr_median_hist_cos','corr_median_hist_euc','corr_median_net_cos','corr_median_net_euc'])

    conn =  create_db_connection("database/database.db")
    cur = conn.cursor()

    query = "SELECT external_code, corr_mean_hist_cos,corr_mean_hist_euc,corr_mean_net_cos,corr_mean_net_euc,corr_median_hist_cos,corr_median_hist_euc,corr_median_net_cos,corr_median_net_euc FROM correlations"
    cur.execute(query)
    res = cur.fetchall()

    for row in res:
        wrt.writerow(row)

    conn.close()