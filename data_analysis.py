import sys
import skimage.io
from scipy.spatial import distance
from matplotlib import pyplot as plt
import numpy as np
import datetime
import queue
from threading import Thread
import os
from dateutil.relativedelta import relativedelta
import sqlite3
from sqlite3 import Error
from resnet import FeaturesExtractor
import cv2
import path_utils
from dateutil.relativedelta import relativedelta
import csv
from threading import Lock
from scipy.stats import linregress
from db_utils import create_db_connection
    
conn =  create_db_connection("database/database.db")
cur = conn.cursor()

query_find = """SELECT  external_code, 
                        color, 
                        category,
                        from_date,
                        to_date,
                        AVG(hist_cosine) AS avg_hist_cosine, 
                        AVG(hist_euclidean) AS avg_hist_euclidean,
                        AVG(net_cosine) AS avg_net_cosine, 
                        AVG(net_euclidean) AS avg_net_euclidean 
                FROM distances
                GROUP BY external_code, color, category,from_date,to_date"""

cur.execute(query_find)
res = cur.fetchall()

max_count = 0
last_code = res[0][0]

hist_cos_list = []
hist_euc_list = []
net_cos_list = []
net_euc_list = []
x_axis = []

for row in res:
    max_count+=1

    if(row[0]!=last_code):
        fig, axs = plt.subplots(2,2,figsize=(15,10))
        fig.autofmt_xdate()
        fig.suptitle(" ".join(row[0:3]))

        x_valrange = range(0,13)

        axs[0,0].plot(x_axis, hist_cos_list)
        #axs[0,0].set_ylim((0.7,1.2))
        #coef = np.polyfit(x_valrange,hist_cos_list,1)
        #poly1d_fn = np.poly1d(coef) 
        #axs[0,0].plot(x_axis, hist_cos_list, 'y', x_valrange, poly1d_fn(x_valrange), '--k')
        axs[0,0].set_title("Histogram cosine")

        axs[0,1].plot(x_axis, hist_euc_list)
        #axs[0,1].set_ylim((0.7,1.2))
        #coef = np.polyfit(x_valrange,hist_euc_list,1)
        #poly1d_fn = np.poly1d(coef) 
        #axs[0,1].plot(x_axis, hist_euc_list, 'y', x_valrange, poly1d_fn(x_valrange), '--k')
        axs[0,1].set_title("Histogram euclidean")

        axs[1,0].plot(x_axis, net_cos_list)
        #axs[1,0].set_ylim((0.002,0.0035))
        #coef = np.polyfit(x_valrange,net_cos_list,1)
        #poly1d_fn = np.poly1d(coef) 
        #axs[1,0].plot(x_axis, net_cos_list, 'y', x_valrange, poly1d_fn(x_valrange), '--k')
        axs[1,0].set_title("Classifier cosine")

        axs[1,1].plot(x_axis, net_euc_list)
        #axs[1,1].set_ylim((1, 2))
        #coef = np.polyfit(x_valrange,net_euc_list,1)
        #poly1d_fn = np.poly1d(coef) 
        #axs[1,1].plot(x_axis, net_euc_list, 'y', x_valrange, poly1d_fn(x_valrange), '--k')
        axs[1,1].set_title("Classifier euclidean")

        fig.savefig("plots/"+"_".join(row[0:3]))
        #plt.show()

        x_axis = []
        hist_cos_list = []
        hist_euc_list = []
        net_cos_list = []
        net_euc_list = []

    x_axis.append(row[4])
    hist_cos_list.append(row[5])
    hist_euc_list.append(row[6])
    net_cos_list.append(row[7])
    net_euc_list.append(row[8])

    """ if(max_count==14):
        break """

    last_code = row[0]
    print(last_code)

conn.close()
