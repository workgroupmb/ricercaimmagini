import sys
import skimage.io
from skimage.viewer import ImageViewer
from scipy.spatial import distance
from matplotlib import pyplot as plt
import numpy as np
import datetime
import queue
from threading import Thread
import os
from dateutil.relativedelta import relativedelta
import sqlite3
from sqlite3 import Error
from resnet import FeaturesExtractor
import cv2
import path_utils
from dateutil.relativedelta import relativedelta
import csv
from threading import Lock
from db_utils import create_db_connection

db_lock = Lock()

# Parametri di base
start_period = datetime.datetime(2015, 1, 1)
end_period = datetime.datetime(2019, 12, 31)
interval = 1

extractor = FeaturesExtractor()

# Inizializza il database ricreando la tabella
def init_db(conn):
    #drop_table = "DROP TABLE IF EXISTS distances"
    create_table = """
    CREATE TABLE IF NOT EXISTS distances (
        cmp_path VARCHAR(255),
        img_path VARCHAR(255),
        external_code VARCHAR(255) NOT NULL,
        keyword VARCHAR(255) NOT NULL,
        color VARCHAR(255) NOT NULL,
        category VARCHAR(255) NOT NULL,
        from_date TEXT NOT NULL,
        to_date TEXT NOT NULL,
        hist_cosine DOUBLE,
        hist_euclidean DOUBLE, 
        net_cosine DOUBLE,
        net_euclidean DOUBLE,
        PRIMARY KEY(cmp_path, img_path)
    );
    """

    try:
        c = conn.cursor()
        c.execute(create_table)
        conn.commit()
    except Error as e:
        print(e)

def get_images(keywords, start_date, end_date):
    path = path_utils.get_full_path(keywords, start_date, end_date)
    
    if(path_utils.is_path_downloaded(keywords, start_date, end_date, 100)):
        return path, os.listdir(path).replace(" ","_")
    return "", []

# Legge l'immagine nella path specificata
def read_image(path):
    try:
        return skimage.io.imread(fname=path)
    except:
        return None

# Estrae l'istogramma RGB da un'immagine, se l'immagine
# non possiede 3 canali allora verrà resistuito None
def extract_hist(path):
    image = read_image(path)

    if(image is not None):
        # tuple to select colors of each channel line
        colors = ("red", "green", "blue")
        channel_ids = (0, 1, 2)

        histogram = np.empty(1)
        try:
            for channel_id, c in zip(channel_ids, colors):
                h, bin_edges = np.histogram(
                    image[:, :, channel_id], bins=256, range=(0, 256)
                )
                histogram = np.concatenate((histogram, h))
        except:
            print("Image is not valid")
            return None

        totsum = np.sum(histogram)/3
        histogram = histogram / totsum

        return histogram
    return None

# Calcola le distanze (coseno ed euclidea) tra 2 liste di numeri
def calculate_distances(data1, data2):
    try:
        if(data1 is not None and data2 is not None):
            return (distance.cosine(data1, data2), distance.euclidean(data1,data2))
        else:
            return (None, None)
    except:
        return (None, None)        

# Estrae tutte le feature da un'immagine locata nella
# path specificata
def extract_features_values(path):
    try:
        img = cv2.imread(path)
        img = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
        return extractor.getFeatures(img)
    except Exception as e:
        print(e)
        return None

# Confronta l'immagine di esempio con tutte le immagini
# che corrispono a quella cartella di tutto il periodo
def compare_images(conn, external_code, keywords, cmp_path, from_date, to_date):
    print(">>",external_code, keywords, cmp_path, from_date, to_date)

    # Se esiste procedi
    if(len(cmp_path)>0):
        # Estrai l'istogramma per i confronti successivi
        cmp_hist = extract_hist(cmp_path)
        # Estrai le feature per i confronti successivi
        cmp_net = extract_features_values(cmp_path)
        print("CMP NET",cmp_net)

        week_start_date = datetime.datetime(from_date.year, from_date.month, 1)
        week_end_date = week_start_date + relativedelta(days=7)
        # Itera tutto il periodo
        while(week_start_date < to_date):
            week_start_date_str = week_start_date.strftime("%Y-%m-%d")
            week_end_date_str = week_end_date.strftime("%Y-%m-%d")
            print("Comparing",external_code,week_start_date_str,week_end_date_str)
            
            path, img_list = get_images([]+keywords,week_start_date_str,week_end_date_str)
            print(img_list)
            # Per ogni immagine trovata in quel periodo 
            # con la ricerca specificata
            for filename in img_list:
                img_path = path+"/"+filename
                print("Analyzing",img_path)
                
                cur = conn.cursor()

                # Controlla che la riga non esista già
                query_find = """SELECT COUNT(*) FROM distances WHERE cmp_path='{}' AND img_path='{}'""".format(cmp_path,img_path)
                cur.execute(query_find)
                num_rows = cur.fetchone()[0]

                # Vai avanti se non esiste
                if(num_rows==0):
                    # Leggi l'immagine, estrai l'istogramma e le feature
                    hist = extract_hist(img_path)
                    net = extract_features_values(img_path)
                    print("NET",net)
                    # Calcola le distanze tra gli istogrammi e tra i set di feature estratti
                    hist_cosine, hist_euclidean = calculate_distances(cmp_hist, hist)
                    net_cosine, net_euclidean = calculate_distances(cmp_net, net)

                    print(hist_cosine, hist_euclidean, net_cosine, net_euclidean)
                    
                    #SCRITTURA SU DATABASE
                    if(None not in [hist_cosine, hist_euclidean, net_cosine, net_euclidean]):
                        sql = '''INSERT INTO distances(cmp_path, img_path, external_code, keyword,color,category,from_date,to_date,hist_cosine,hist_euclidean,net_cosine,net_euclidean)
                                VALUES(?,?,?,?,?,?,?,?,?,?,?,?); '''
                        kw=set(keywords) & set(search_keywords)
                        cl=set(keywords) & set(color_labels)
                        ct=set(keywords) & set(categ_labels)
                        
                        db_lock.acquire()
                        cur.execute(sql, (
                                cmp_path,
                                img_path,
                                external_code,
                                kw.pop() if kw else None,
                                cl.pop() if cl else None,
                                ct.pop() if ct else None,
                                week_start_date_str,
                                week_end_date_str,
                                hist_cosine, hist_euclidean,
                                net_cosine, net_euclidean
                            )
                        )
                        conn.commit()
                        db_lock.release()
                        print("Saved to database")
                else:
                    print("-- Skipping, already calculated...")
            week_start_date += relativedelta(months=interval)
            week_end_date += relativedelta(months=interval)

def thread_compare_images(q):
    conn =  create_db_connection("database/database.db")
    while True:
        params = q.get()
        compare_images(conn, *params)
        q.task_done()

search_keywords = ['fashionable','trendy','top-rated']
categ_labels = ['doll dress','kimono dress','long dress','sheath dress','shirt dress','trapeze dress'] # 6
color_labels = ['teal', 'bordeaux', 'pink', 'yellow', 'sky', 'gold', 'red', 'black cherry', 'grey', 'coral', 'white', 'oil', 'pearl', 'fuchsia', 'silver', 'pattern', 'face powder', 'military green', 'royal', 'camel', 'black', 'mustard', 'avion', 'indigo', 'jeans', 'mud', 'green', 'cream', 'strawberry', 'purple', 'beige', 'blue', 'russet'] # 33

# Inizializza la coda
queue = queue.Queue(maxsize=1)
num_threads = 1

conn =  create_db_connection("database/database.db")
# create tables
if conn is not None:
    init_db(conn)
    conn.close()
else:
    print("Error! cannot create the database connection.")

# Fai partire i worker 
for i in range(num_threads):
    worker = Thread(target=thread_compare_images, args=(queue,))
    worker.setDaemon(True)
    worker.start()

# Read the dataset
with open('list_products/list_products.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')

    for n, row in enumerate(csv_reader):
        if(n>0):
            external_code = row[1]
            img_date = datetime.datetime.strptime(row[4], "%Y-%m-%d")
            cmp_path = "list_products/images_clean/"+row[5]

            category = row[2]
            color = row[3]

            from_date = img_date - relativedelta(years=1)
            to_date = img_date 
            
            for keyword in search_keywords:
                queue.put([external_code, [keyword, color, category], cmp_path, from_date, to_date])

queue.join()