from scipy import stats
from db_utils import create_db_connection
import csv
import numpy as np

def init_db(conn):
    drop_table = "DROP TABLE IF EXISTS correlations"
    create_table = """
        CREATE TABLE IF NOT EXISTS correlations (
            external_code VARCHAR(255) PRIMARY KEY,
            corr_mean_hist_cos DOUBLE,
            corr_mean_hist_euc DOUBLE,
            corr_mean_net_cos DOUBLE,
            corr_mean_net_euc DOUBLE,
            corr_median_hist_cos DOUBLE,
            corr_median_hist_euc DOUBLE,
            corr_median_net_cos DOUBLE,
            corr_median_net_euc DOUBLE
        );
    """

    try:
        c = conn.cursor()
        c.execute(drop_table)
        c.execute(create_table)
        conn.commit()
    except Error as e:
        print(e)
        exit(0)

def calculate_pearson(data1, data2):
    try:
        return stats.pearsonr(data1, data2)
    except Exception as e:
        print("Error:",e)
        return (None, None)


def get_mean_values(ext_code):
    query_find = """
        SELECT * FROM (
            SELECT  d.from_date,
                    AVG(hist_cosine) AS avg_hist_cosine, 
                    AVG(hist_euclidean) AS avg_hist_euclidean,
                    AVG(net_cosine) AS avg_net_cosine, 
                    AVG(net_euclidean) AS avg_net_euclidean 
            FROM distances d
            WHERE external_code = '{}'
            GROUP BY d.from_date
            ORDER BY d.from_date DESC LIMIT 12
        ) ORDER BY from_date ASC""".format(ext_code)
    cur.execute(query_find)
    mean_res = cur.fetchall()
    
    # If there is a valid result
    if(len(mean_res)>0):
        hist_cos_list = []
        hist_euc_list = []
        net_cos_list = []
        net_euc_list = []

        for i, month in enumerate(mean_res):
            hist_cos_list.append(month[1])
            hist_euc_list.append(month[2])
            net_cos_list.append(month[3])
            net_euc_list.append(month[4])
        return hist_cos_list,hist_euc_list,net_cos_list,net_euc_list
    else:
        return None, None, None, None


def get_median_values(ext_code):
    query_find = """
        SELECT  from_date,
                hist_cosine, 
                hist_euclidean,
                net_cosine, 
                net_euclidean 
        FROM distances d
        WHERE external_code = '{}' AND d.from_date IN (
            SELECT d2.from_date
            FROM distances d2
            WHERE external_code = '{}'
            GROUP BY d2.from_date
            ORDER BY d2.from_date DESC LIMIT 12
        )
        ORDER BY from_date ASC""".format(ext_code, ext_code)
    cur.execute(query_find)
    median_res = cur.fetchall()
    
    # If there is a valid result
    if(len(median_res)>0):
        last_date = median_res[0][0]

        hist_cos_list = []
        hist_euc_list = []
        net_cos_list = []
        net_euc_list = []

        med_hist_cos_list = []
        med_hist_euc_list = []
        med_net_cos_list = []
        med_net_euc_list = []

        for i, row in enumerate(median_res):
            is_last = (i >= (len(median_res)-1))

            if((last_date is None or row[0]==last_date) and not is_last):
                hist_cos_list.append(row[1])
                hist_euc_list.append(row[2])
                net_cos_list.append(row[3])
                net_euc_list.append(row[4])
            else:
                if(is_last):
                    hist_cos_list.append(row[1])
                    hist_euc_list.append(row[2])
                    net_cos_list.append(row[3])
                    net_euc_list.append(row[4])
                    
                med_hist_cos_list.append(np.median(hist_cos_list))
                med_hist_euc_list.append(np.median(hist_euc_list))
                med_net_cos_list.append(np.median(net_cos_list))
                med_net_euc_list.append(np.median(net_euc_list))

                last_date = row[0]
                hist_cos_list = [row[1]]
                hist_euc_list = [row[2]]
                net_cos_list = [row[3]]
                net_euc_list = [row[4]]

        if(len(med_hist_cos_list)==12 and len(med_hist_euc_list)==12 and len(med_net_cos_list)==12 and len(med_net_euc_list)==12):
            return med_hist_cos_list,med_hist_euc_list,med_net_cos_list,med_net_euc_list
    return None, None, None, None

conn =  create_db_connection("database/database.db")
cur = conn.cursor()
# create tables
if conn is not None:
    init_db(conn)
else:
    print("Error! cannot create the database connection.")

#get_mean_values("AM1377CC179FA-17")
# Read the dataset
with open('product_sell/sell_dataset.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')

    for n, row in enumerate(csv_reader):
        if(n>0):
            ext_code = row[0]

            # Controlla che la riga non esista già
            query_find = """SELECT COUNT(*) FROM correlations WHERE external_code='{}'""".format(ext_code)
            cur.execute(query_find)
            num_rows = cur.fetchone()[0]

            # Vai avanti se non esiste
            if(num_rows==0):
                sell_data = np.array(row[1:13])
                sell_data = sell_data.astype(np.float)

                # MEAN VALUES
                mean_hist_cos_list,mean_hist_euc_list,mean_net_cos_list,mean_net_euc_list = get_mean_values(ext_code)

                print("1.",mean_hist_cos_list,mean_hist_euc_list,mean_net_cos_list,mean_net_euc_list)
                if(None not in [mean_hist_cos_list,mean_hist_euc_list,mean_net_cos_list,mean_net_euc_list]):
                    corr_mean_hist_cos, _ = calculate_pearson(mean_hist_cos_list, sell_data)
                    corr_mean_hist_euc, _ = calculate_pearson(mean_hist_euc_list, sell_data)
                    corr_mean_net_cos, _ = calculate_pearson(mean_net_cos_list, sell_data)
                    corr_mean_net_euc, _ = calculate_pearson(mean_net_euc_list, sell_data)            
                    print("2.",corr_mean_hist_cos, corr_mean_hist_euc, corr_mean_net_cos, corr_mean_net_euc)

                    # MEDIAN VALUES
                    median_hist_cos_list,median_hist_euc_list,median_net_cos_list,median_net_euc_list = get_median_values(ext_code)        
                    print("3.",median_hist_cos_list,median_hist_euc_list,median_net_cos_list,median_net_euc_list)

                    if(None not in [median_hist_cos_list,median_hist_euc_list,median_net_cos_list,median_net_euc_list]):
                        corr_median_hist_cos, _ = calculate_pearson(median_hist_cos_list, sell_data)
                        corr_median_hist_euc, _ = calculate_pearson(median_hist_euc_list, sell_data)
                        corr_median_net_cos, _ = calculate_pearson(median_net_cos_list, sell_data)
                        corr_median_net_euc, _ = calculate_pearson(median_net_euc_list, sell_data)

                        print("4.",corr_median_hist_cos, corr_median_hist_euc, corr_median_net_cos, corr_median_net_euc)

                        if(None not in [corr_mean_hist_cos,corr_mean_hist_euc,corr_mean_net_cos,corr_mean_net_euc,corr_median_hist_cos,corr_median_hist_euc,corr_median_net_cos,corr_median_net_euc]):
                            insert = '''INSERT INTO correlations(external_code, corr_mean_hist_cos,corr_mean_hist_euc,corr_mean_net_cos,corr_mean_net_euc,corr_median_hist_cos,corr_median_hist_euc,corr_median_net_cos,corr_median_net_euc)
                                    VALUES(?,?,?,?,?,?,?,?,?); '''

                            cur.execute(insert, (
                                ext_code,
                                corr_mean_hist_cos,
                                corr_mean_hist_euc,
                                corr_mean_net_cos,
                                corr_mean_net_euc,
                                corr_median_hist_cos,
                                corr_median_hist_euc,
                                corr_median_net_cos,
                                corr_median_net_euc
                            ))
                            conn.commit()
                            print("Saved to database", ext_code)
                print("--------------------------------")
conn.close()