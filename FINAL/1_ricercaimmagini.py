# DOWNLOAD FROM JANUARY 2015 TO DECEMBER 2019 WITH WEEKLY TIMEFRAME
""" 
dict_keys(['capris', 'cloak', 'culottes', 'doll dress', 'drop sleeve', 'gitana skirt', 'kimono dress', 'long cardigan', 'long coat', 'long dress', 'long duster', 'long skirt', 'long sleeve', 'longuette skirt', 'maxi', 'medium cardigan', 'medium coat', 'medium duster', 'midi skirt', 'miniskirt', 'patterned', 'printed', 'sheath dress', 'shirt dress', 'short cardigan', 'short coat', 'short sleeves', 'shorts', 'sleeveless', 'solid colours', 'tracksuit', 'trapeze dress']) 32
dict_keys(['avion', 'azure', 'beige', 'black', 'black cherry', 'blue', 'bordeaux', 'bronze', 'brown', 'camel', 'coral', 'cream', 'face powder', 'fuchsia', 'gold', 'green', 'grey', 'ice', 'indigo', 'jeans', 'lead', 'leather', 'maldives', 'marsala', 'military green', 'mud', 'mustard', 'oil', 'onion', 'orange', 'pattern', 'peach', 'pearl', 'pink', 'purple', 'red', 'royal', 'russet', 'silver', 'sky', 'strawberry', 'teal', 'white', 'yellow']) 44
"""

import torch
import datetime
from image_downloader import image_downloader as imgd
import queue
from threading import Thread
from dateutil.relativedelta import relativedelta
from lxml.html import fromstring
import requests
import re
import os
import threading
import csv
import time
import path_utils

downloaded_folders = 0

def get_proxies():
    url = 'https://free-proxy-list.net/'
    response = requests.get(url)
    reg = r'\d+.\d+.\d+.\d+:\d+'
    print(re.findall(reg, response.text))
    proxies = re.findall(reg, response.text)
    print(proxies)  
    return proxies

# DOWNLOAD ALL THE IMAGES
def download_images(keywords=[], from_date="", to_date="", limit=100, extensions={'.jpg', '.png', '.jpeg'}, proxy=""):
    global downloaded_folders

    path = path_utils.get_full_path(keywords, from_date, to_date)
    downloaded = path_utils.is_path_downloaded(keywords, from_date, to_date, limit)

    # ADD BEFORE DATE IF EXISTS
    if(len(to_date)>0):
        keywords.append("before:"+to_date)

    # ADD AFTER DATE IF EXISTS
    if(len(from_date)>0):
        keywords.append("after:"+from_date)

    # DOWNLOAD THE IMAGES
    if(not downloaded and len(keywords)>0):
        downloaded_folders += 1

        keywords_str = " ".join(keywords)
        print("Downloading", keywords_str)
        imgd().download(keywords_str, limit, path=path, extensions=extensions, proxy=proxy)
    else:
        print(">> Folder already downloaded, moving forward >>")

# MULTI THREAD DOWNLOADING
def download_images_thread(q, num):
    global proxies
    if(len(proxies)>0):
        proxy = proxies[num]
    else:
        proxy = ""

    print("Starting thread", num, "proxy", proxy)

    while True:
        # TAKE ALL THE PARAMETERS FROM THE QUEUE
        params = q.get()

        download_images(keywords=params[0], from_date=params[1], to_date=params[2], proxy=proxy)
        print("Thread",num,"proxy",proxy,"completed the task")

        # RETURN THAT TASK IS DONE
        q.task_done()


def iterate_keyword_period(keywords, queue):
    global start_period
    global end_period

    from_date = start_period
    to_date = start_period + datetime.timedelta(days=7)
    while(to_date <= end_period):
        from_date_str = from_date.strftime("%Y-%m-%d") 
        to_date_str = to_date.strftime("%Y-%m-%d") 

        queue.put(([]+keywords, from_date_str, to_date_str))

        # INCREMENT THE PERIOD
        from_date += relativedelta(months=interval)
        to_date += relativedelta(months=interval)


# BASE KEYWORDS
search_keywords = ["fashionable", "top-rated", "trendy"]

categ_labels = ['doll dress','kimono dress','long dress','sheath dress','shirt dress','trapeze dress'] # 6
color_labels = ['teal', 'bordeaux', 'pink', 'yellow', 'sky', 'gold', 'red', 'black cherry', 'grey', 'coral', 'white', 'oil', 'pearl', 'fuchsia', 'silver', 'pattern', 'face powder', 'military green', 'royal', 'camel', 'black', 'mustard', 'avion', 'indigo', 'jeans', 'mud', 'green', 'cream', 'strawberry', 'purple', 'beige', 'blue', 'russet'] # 33

print(categ_labels, len(categ_labels))
print(color_labels, len(color_labels))

download_completed = False

# BASE PARAMETERS
start_period = datetime.datetime(2015, 1, 1)
end_period = datetime.datetime(2019, 12, 31)
interval = 1

proxies = get_proxies()

# STARTS THE QUEUE
q = queue.Queue(maxsize=0)
num_threads = len(proxies)


# STARTS THE WORKER 
for i in range(num_threads):
    worker = Thread(target=download_images_thread, args=(q,i,))
    worker.setDaemon(True)
    worker.start()



while not download_completed:
    downloaded_folders = 0

    # FIND ALL COMBINATIONS
    for keyword in search_keywords:
        iterate_keyword_period([keyword, "clothing"], q)

        for categ in categ_labels:
            iterate_keyword_period([keyword, categ], q)

            for color in color_labels:
                iterate_keyword_period([keyword, color, categ], q)


    # WAIT UNTIL QUEUE IS EMPTY
    q.join()

    if(downloaded_folders==0):
        download_completed = True 