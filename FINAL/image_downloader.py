import os
import time
import urllib
import requests
import magic
import progressbar
import threading
from urllib.parse import quote
from functools import wraps
import subprocess
    
safe_download_counter = 0
counter_lock = threading.Lock()
max_download = 300
max_period_sec = 5

# COUNT HOW MUCH IMAGES HAS BEEN DOWNLOADED AND WAIT IN CASE OF EXCEEDING
def safe_counter_process():
    global safe_download_counter
    global counter_lock
    global max_download
    global max_period_sec

    while True:
        prev = safe_download_counter
        counter_lock.acquire()
        safe_download_counter = max(0, safe_download_counter-max_download)
        counter_lock.release()

        print("Decreasing",prev, safe_download_counter)
        time.sleep(max_period_sec)

t = threading.Thread(target=safe_counter_process)
t.start()
# CHANGE COUNTER NUMBER SAFELY    
def counter_edit(amount):
    global counter_lock
    global safe_download_counter

    counter_lock.acquire()
    safe_download_counter += amount
    counter_lock.release()

class image_downloader:
    def __init__(self):
        pass
    # RETRIEVE ALL URLS FROM A SEARCH
    def urls(self, keywords, limit, extensions={'.jpg', '.png', '.ico', '.gif', '.jpeg'}, proxy=""):
        keyword_to_search = [str(item).strip() for item in keywords.split(',')]
        i = 0
        links = []

        things = len(keyword_to_search) * limit

        bar = progressbar.ProgressBar(maxval=things, \
                                      widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()]).start()

        while i < len(keyword_to_search):
            url = 'https://www.google.com/search?q=' + quote(
                keyword_to_search[i].encode(
                    'utf-8')) + '&biw=1536&bih=674&tbm=isch&sxsrf=ACYBGNSXXpS6YmAKUiLKKBs6xWb4uUY5gA:1581168823770&source=lnms&sa=X&ved=0ahUKEwioj8jwiMLnAhW9AhAIHbXTBMMQ_AUI3QUoAQ'
            raw_html = self._download_page(url)

            end_object = -1;
            google_image_seen = False;
            j = 0

            while j < limit:
                while (True):
                    try:
                        new_line = raw_html.find('"https://', end_object + 1)
                        end_object = raw_html.find('"', new_line + 1)

                        buffor = raw_html.find('\\', new_line + 1, end_object)
                        if buffor != -1:
                            object_raw = (raw_html[new_line + 1:buffor])
                        else:
                            object_raw = (raw_html[new_line + 1:end_object])

                        if any(extension in object_raw for extension in extensions):
                            break

                    except Exception as e:
                        break


                try:
                    r = requests.get(object_raw, allow_redirects=True, timeout=1,proxies={"http": proxy})
                    if('html' not in str(r.content)):
                        mime = magic.Magic(mime=True)
                        file_type = mime.from_buffer(r.content)
                        file_extension = f'.{file_type.split("/")[1]}'
                        if file_extension == '.png' and not google_image_seen:
                            google_image_seen = True
                            raise ValueError();
                        links.append(object_raw)
                        bar.update(bar.currval + 1)
                    else:
                        j -= 1
                except Exception as e:
                    j -= 1
                j += 1

            i += 1

        bar.finish()
        return(links)

    # DOWNLOAD ALL IMAGES OF A SEARCH
    def download(self, keywords, limit, path="", extensions={'.jpg', '.png', '.ico', '.gif', '.jpeg'},proxy=""):
        global safe_download_counter
        global max_download

        keyword_to_search = [str(item).strip() for item in keywords.split(',')]
        
        if(path==""):
            main_directory = "images/" + keyword_to_search[i].replace(" ", "_")
        else:
            main_directory = path

        i = 0

        things = len(keyword_to_search) * limit

        bar = progressbar.ProgressBar(maxval=things, \
                                      widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])

        bar.start()

        while i < len(keyword_to_search):
            self._create_directories(main_directory)
            url = 'https://www.google.com/search?q=' + quote(
                keyword_to_search[i].encode('utf-8')) + '&biw=1536&bih=674&tbm=isch&sxsrf=ACYBGNSXXpS6YmAKUiLKKBs6xWb4uUY5gA:1581168823770&source=lnms&sa=X&ved=0ahUKEwioj8jwiMLnAhW9AhAIHbXTBMMQ_AUI3QUoAQ'
            raw_html = self._download_page(url)

            end_object = -1
            google_image_seen = False
            j = 0
            while j < limit:
                # Wait for the safe counter to go below max_download
                while(safe_download_counter>=max_download):
                    pass

                counter_edit(1)
                
                while (True):
                    try:
                        new_line = raw_html.find('"https://', end_object + 1)
                        end_object = raw_html.find('"', new_line + 1)

                        buffor = raw_html.find('\\', new_line + 1, end_object)
                        if buffor != -1:
                            object_raw = (raw_html[new_line+1:buffor])
                        else:
                            object_raw = (raw_html[new_line+1:end_object])

                        if any(extension in object_raw for extension in extensions):
                            break

                    except Exception as e:
                        break

                path = main_directory

                try:
                    r = requests.get(object_raw, allow_redirects=True, timeout=1,proxies={"http": proxy})
                    if('html' not in str(r.content)):
                        mime = magic.Magic(mime=True)
                        file_type = mime.from_buffer(r.content)
                        file_extension = f'.{file_type.split("/")[1]}'
                        if file_extension not in extensions:
                            raise ValueError()
                        if file_extension == '.png' and not google_image_seen:
                            google_image_seen = True
                            raise ValueError()
                        file_name = str(keyword_to_search[i]) + "_" + str(j + 1) + file_extension
                        file_name.replace(":","@")
                        with open(os.path.join(path, file_name), 'wb') as file:
                            file.write(r.content)
                        bar.update(bar.currval + 1)
                    else:
                        j -= 1
                        counter_edit(-1)
                except Exception as e:
                    j -= 1
                    counter_edit(-1)
                j += 1

            i += 1
        bar.finish()

    # CREATE DIRECTORIE IN CASE OF A MISSING ONE
    def _create_directories(self, main_directory):
        try:
            if not os.path.exists(main_directory):
                os.makedirs(main_directory)
                time.sleep(0.2)
            else:
                sub_directory = main_directory
                if not os.path.exists(sub_directory):
                    os.makedirs(sub_directory)

        except OSError as e:
            if e.errno != 17:
                raise
            pass
        return

    # READ THE HTML PAGE FOR FURTHER PARSING
    def _download_page(self,url):
        try:
            headers = {}
            headers['User-Agent'] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36"
            req = urllib.request.Request(url, headers=headers)
            resp = urllib.request.urlopen(req)
            respData = str(resp.read())
            return respData

        except Exception as e:
            print(e)
            exit(0)