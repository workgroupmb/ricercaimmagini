from scipy import stats
from db_utils import create_db_connection
import csv
import numpy as np
import sys
import matplotlib.pyplot as plt

conn =  create_db_connection("database/database.db")
cur = conn.cursor()

# READ THE DATASET
with open('product_sell/sell_dataset.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')

    sell_list = []
    x_list = []

    for n, row in enumerate(csv_reader):
        if(n>0):
            # GET THE EXTERNAL CODE ( PRODUCT ID )
            ext_code = row[0]
            name = row[14]

            print(ext_code, name)

            if("dress" in name.lower()):
                # EXTRACT THE 12 MONTHS SELL DATA AND SUM IT
                sell_data = np.array(row[1:13])
                sell_data = sell_data.astype(np.float)
                # APPEND IT TO THE LIST TO CREATE THE SIGNAL
                sell_list.append(np.sum(sell_data))
                x_list.append(ext_code)


plt.figure(figsize=(10,4))
plt.plot(sell_list)
plt.savefig("product_sell/sell_sum")
plt.show()



conn.close()
