import sys
import skimage.io
from scipy.spatial import distance
from matplotlib import pyplot as plt
import numpy as np
import datetime
import queue
from threading import Thread
import os
from dateutil.relativedelta import relativedelta
import sqlite3
from sqlite3 import Error
from resnet import FeaturesExtractor
import cv2
import path_utils
from dateutil.relativedelta import relativedelta
import csv
from threading import Lock
from scipy.stats import linregress
from db_utils import create_db_connection
    
conn =  create_db_connection("database/database.db")
cur = conn.cursor()


# QUERY TO SELECT ALL DISTANCES
query_find = """SELECT  external_code, 
                        color, 
                        category,
                        from_date,
                        to_date,
                        AVG(hist_cosine) AS avg_hist_cosine, 
                        AVG(hist_euclidean) AS avg_hist_euclidean,
                        AVG(net_cosine) AS avg_net_cosine, 
                        AVG(net_euclidean) AS avg_net_euclidean 
                FROM distances
                GROUP BY external_code, color, category,from_date,to_date"""

cur.execute(query_find)
res = cur.fetchall()

max_count = 0
last_code = res[0][0]

hist_cos_list = []
hist_euc_list = []
net_cos_list = []
net_euc_list = []
x_axis = []

if not (os.path.exists("plots")):
    os.makedirs("plots")

for row in res:
    max_count+=1
    # WHILE THE CODE DOESN'T CHANGE
    if(row[0]!=last_code):
        fig, axs = plt.subplots(2,2,figsize=(15,10))
        fig.autofmt_xdate()
        fig.suptitle(" ".join(row[0:3]))

        x_valrange = range(0,13)

        axs[0,0].plot(x_axis, hist_cos_list)
        axs[0,0].set_title("Histogram cosine")

        axs[0,1].plot(x_axis, hist_euc_list)
        axs[0,1].set_title("Histogram euclidean")

        axs[1,0].plot(x_axis, net_cos_list)
        axs[1,0].set_title("Classifier cosine")

        axs[1,1].plot(x_axis, net_euc_list)
        axs[1,1].set_title("Classifier euclidean")
        fig.savefig("plots/"+"_".join(row[0:3]))

        x_axis = []
        hist_cos_list = []
        hist_euc_list = []
        net_cos_list = []
        net_euc_list = []

    # WHEN THE CODE CHANGES

    x_axis.append(row[4])
    hist_cos_list.append(row[5])
    hist_euc_list.append(row[6])
    net_cos_list.append(row[7])
    net_euc_list.append(row[8])

    last_code = row[0]
    print(last_code)

conn.close()
