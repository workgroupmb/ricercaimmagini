import os 

def get_file_name(keywords=[], from_date="", to_date=""):
    keywords = []+keywords

    # ADD BEFORE DATE IF EXISTS
    if(len(to_date)>0):
        keywords.append("before:"+to_date)

    # ADD END DATE IF EXISTS
    if(len(from_date)>0):
        keywords.append("after:"+from_date)

    folder_title = "_".join(keywords).replace(" ", "_")

    return folder_title.replace(":","@")

def get_path(keywords=[], from_date="", to_date=""):
    keywords = []+keywords

    folder_name = "/".join(keywords)

    # ADD BEFORE DATE IF EXISTS
    if(len(to_date)>0):
        keywords.append("before:"+to_date)

    # ADD END DATE IF EXISTS
    if(len(from_date)>0):
        keywords.append("after:"+from_date)

    # CHECK IF THE FOLDER EXISTS AND CONTAINSAT LEAST "LIMIT" IMAGES
    return "images/"+(folder_name.replace(":","@"))

def get_full_path(keywords=[], from_date="", to_date=""):
    return get_path(keywords, from_date, to_date)+"/"+get_file_name(keywords, from_date, to_date)



def is_path_downloaded(keywords=[], from_date="", to_date="", limit=0):
    keywords = []+keywords

    path = get_full_path(keywords, from_date, to_date)

    downloaded = False
    if(os.path.isdir(path)):
        nFiles = len(os.listdir(path))
        downloaded = (nFiles>=limit)

    return downloaded


