import os
import re
import torch 
import shutil

categories = ['doll dress','kimono dress','long dress','sheath dress','shirt dress','trapeze dress'] # 6
colors = ['teal', 'bordeaux', 'pink', 'yellow', 'sky', 'gold', 'red', 'black cherry', 'grey', 'coral', 'white', 'oil', 'pearl', 'fuchsia', 'silver', 'pattern', 'face powder', 'military green', 'royal', 'camel', 'black', 'mustard', 'avion', 'indigo', 'jeans', 'mud', 'green', 'cream', 'strawberry', 'purple', 'beige', 'blue', 'russet'] # 33

keywords=['fashionable','trendy','top-rated']

path = "images"
fname = []

for root,d_names,f_names in os.walk(path):
    for f in f_names:
        fname.append(os.path.join(root, f))

    for name in fname:
        if(len(name.split("/"))>=4 and (not "clothing" in name) and (not "dress" in name)):
            spl = re.split("/",name)
            print(spl)
            cat_int = len(set(spl) & set(categories))
            col_int = len(set(spl) & set(colors))
            if(not (cat_int>0 or (cat_int>0 and col_int>0))):
                print(cat_int, col_int, "- Removing", name )
                os.remove(name)

    fname = []


categ_labels = torch.load("liste/eng_category_labels.pt").keys()
color_labels = torch.load("liste/eng_color_labels.pt").keys()

for k in keywords:
    for categ in categ_labels:
        path = "images/"+k+"/"+categ
        if(not categ in categories):
            if(os.path.isdir(path)):
                shutil.rmtree(path)

    for color in color_labels:
        if(not color in colors):
            path = "images/"+k+"/"+color
            if(os.path.isdir(path)):
                shutil.rmtree(path)