from scipy import stats
import matplotlib.pyplot as plt

def calculate_pearson(data1, data2):
    try:
        return stats.pearsonr(data1, data2)
    except Exception as e:
        print("Error:",e)
        return (None, None)



a = [1,2,3,4,5,6,7,8,9,10,11,12,13,14]
b = [14,13,12,11,10,9,8,7,6,5,4,3,2,1]
c = []+b

plt.plot(a)
plt.plot(b)

for i, el in enumerate(c):
    c[i] = 1/el
    print(el, 1/el)

plt.plot(c)
plt.show()

print(calculate_pearson(a,b))