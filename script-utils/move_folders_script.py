import os
import torch
import time
import shutil
from os.path import isfile, join

dirlist = os.listdir("images")

def _create_directories(path):
    try:
        if not os.path.exists(path):
            os.makedirs(path)
            time.sleep(0)

    except OSError as e:
        if e.errno != 17:
            raise
        pass
    return

def move_file(source, target, filename):
    filepath = os.path.join(source, filename)
    if not isfile(os.path.join(target, filename)):
        shutil.move(filepath, target)
    else:
        os.remove(filepath)

def move_folders(keywords=[], scr_path=""):
    global dirlist
    if(scr_path!=""):
        directory = os.listdir("images/"+scr_path)
    else:
        directory = []+dirlist

    target_dir = "images/"+("/".join(keywords))
    _create_directories(target_dir)
    
    name = "_".join(keywords)

    found = False
    for idx, source_dir in enumerate(directory):
        found = False
        if(source_dir.startswith(name)):
            print("> Found", name)
            found = True
            #directory.remove(source_dir)

            full_source_dir = "images/"+scr_path+"/"+source_dir
            files = os.listdir(full_source_dir)

            full_target_dir = target_dir + "/" + source_dir
            _create_directories(full_target_dir)

            for file_name in files:
                move_file(full_source_dir, full_target_dir, file_name)

            os.rmdir(full_source_dir)
    if not found:
        print("Not found", name)

def check_folder(keywords=[]):
    path = "/".join(keywords)

    source_dir = path+"/images"
    if(os.path.isdir("images/"+source_dir)):
        move_folders(keywords, source_dir)
        
    full_source_dir = "images/"+path

    if(os.path.isdir(full_source_dir)):
        files_list = [f for f in os.listdir(full_source_dir) if isfile(join(full_source_dir, f))]
        for f in files_list:
            fs = f.split(".")
            ext = fs[-1]
            fs2 = fs[0].split(" ")
            fs3 = fs2[0:-1] + [fs2[-1].split("_")[0]]

            full_target_dir = "images/"+("/".join(keywords))+"/"+("_".join(fs3))
            _create_directories(full_target_dir)

            move_file(full_source_dir, full_target_dir, f)



search_keywords = ["trendy","top-rated","fashionable"]
categ_labels = torch.load("liste/eng_category_labels.pt").keys()
color_labels = torch.load("liste/eng_color_labels.pt").keys()

print(categ_labels, len(categ_labels))
print(color_labels, len(color_labels))
print(dirlist, len(dirlist))


for k in search_keywords:
    move_folders([k, "clothing"])

    for categ in categ_labels:
        categ = categ.replace(" ", "_")
        move_folders([k, categ])

        for color in color_labels:
            color = color.replace(" ", "_")
            move_folders([k, color, categ])

# Check finale
for k in search_keywords:
    check_folder([k, "clothing"])

    for categ in categ_labels:
        categ = categ.replace(" ", "_")
        check_folder([k, categ])

        for color in color_labels:
            color = color.replace(" ", "_")
            check_folder([k, color, categ])