from scipy import stats
from db_utils import create_db_connection
import csv
import numpy as np
import sys
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler

conn =  create_db_connection("database/database.db")
cur = conn.cursor()

def calculate_pearson(data1, data2):
    try:
        res = stats.pearsonr(data1, data2)
        return res
    except Exception as e:
        print("Error:",e)
        return (None, None)


def get_mean_values(ext_code):
    query_find = """
        SELECT  AVG(avg_hist_euclidean),
                AVG(avg_hist_cosine), 
                AVG(avg_net_euclidean), 
                AVG(avg_net_cosine) 
        FROM (
            SELECT  d.from_date,
                    AVG(hist_cosine) AS avg_hist_cosine, 
                    AVG(hist_euclidean) AS avg_hist_euclidean,
                    AVG(net_cosine) AS avg_net_cosine, 
                    AVG(net_euclidean) AS avg_net_euclidean 
            FROM distances d
            WHERE external_code = '{}'
            GROUP BY d.from_date
            ORDER BY d.from_date DESC LIMIT 12
        )
    """.format(ext_code)

    cur.execute(query_find)
    mean_res = cur.fetchone()
    
    # If there is a valid result
    if(len(mean_res)>0 and 0 not in mean_res and None not in mean_res):
        return mean_res[0], mean_res[1], mean_res[2], mean_res[3]
    else:
        return None, None, None, None


def get_median_values(ext_code):
    query_find = """
        SELECT  d.from_date,
                AVG(hist_euclidean) AS avg_hist_euclidean, 
                AVG(hist_cosine) AS avg_hist_cosine,
                AVG(net_euclidean) AS avg_net_euclidean, 
                AVG(net_cosine) AS avg_net_cosine 
        FROM distances d
        WHERE external_code = '{}'
        GROUP BY d.from_date
        ORDER BY d.from_date DESC LIMIT 12
    """.format(ext_code, ext_code)
    cur.execute(query_find)
    median_res = cur.fetchall()

    # If there is a valid result
    if(len(median_res)>0):
        hist_cos_list = []
        hist_euc_list = []
        net_cos_list = []
        net_euc_list = []

        for i, row in enumerate(median_res):
            hist_euc_list.append(row[1])
            hist_cos_list.append(row[2])
            net_euc_list.append(row[3])
            net_cos_list.append(row[4])

        return np.median(hist_euc_list), np.median(hist_cos_list), np.median(net_euc_list), np.median(net_cos_list)

    return None, None, None, None


#get_mean_values("AM1377CC179FA-17")
# Read the dataset
with open('product_sell/sell_dataset.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')

    sell_list = np.array([])
    avg_hist_euc_list = np.array([])
    avg_hist_cos_list = np.array([])
    avg_net_euc_list = np.array([])
    avg_net_cos_list = np.array([])
    med_hist_euc_list = np.array([])
    med_hist_cos_list = np.array([])
    med_net_euc_list = np.array([])
    med_net_cos_list = np.array([])

    with open('product_sell/signals.csv', 'w') as signals:
        writer = csv.writer(signals, delimiter=',')

        for n, row in enumerate(csv_reader):
            if(n>0):
                # Get the external code (id of product)
                ext_code = row[0]
                name = row[14]

                print(ext_code, name)

                if("dress" in name.lower()):
                    # Extract the average and median values of the single product signal
                    ahe, ahc, ane, anc = get_mean_values(ext_code)
                    mhe, mhc, mne, mnc = get_median_values(ext_code)

                    print([ahe, ahc, ane, anc, mhe, mhc, mne, mnc])

                    if(None not in [ahe, ahc, ane, anc, mhe, mhc, mne, mnc]):

                        # Extract the 12 months sell data and sum it
                        sell_data = np.array(row[1:13])
                        sell_data = sell_data.astype(np.float)
                        sell_sum = np.sum(sell_data)
                        # Append it to the list to create the signal
                        sell_list = np.append(sell_list, sell_sum)

                        # Append them
                        avg_hist_euc_list = np.append(avg_hist_euc_list, ahe)
                        avg_hist_cos_list = np.append(avg_hist_cos_list, ahc)
                        avg_net_euc_list = np.append(avg_net_euc_list, ane)
                        avg_net_cos_list = np.append(avg_net_cos_list, anc)
                        med_hist_euc_list = np.append(med_hist_euc_list, mhe)
                        med_hist_cos_list = np.append(med_hist_cos_list, mhc)
                        med_net_euc_list = np.append(med_net_euc_list, mne)
                        med_net_cos_list = np.append(med_net_cos_list, mnc)

                        val = [ext_code,sell_sum,ahe,ahc,ane,anc,mhe,mhc,mne,mnc]
                        writer.writerow(val)
                    else:
                        break

    scaler = MinMaxScaler()
    
    norm_sell_list = scaler.fit_transform(sell_list[:,np.newaxis])
    norm_avg_hist_euc = scaler.fit_transform(avg_hist_euc_list[:,np.newaxis])
    norm_avg_hist_cos = scaler.fit_transform(avg_hist_cos_list[:,np.newaxis])
    norm_avg_net_euc = scaler.fit_transform(avg_net_euc_list[:,np.newaxis])
    norm_avg_net_cos = scaler.fit_transform(avg_net_cos_list[:,np.newaxis])
    norm_med_hist_euc = scaler.fit_transform(med_hist_euc_list[:,np.newaxis])
    norm_med_hist_cos = scaler.fit_transform(med_hist_cos_list[:,np.newaxis])
    norm_med_net_euc = scaler.fit_transform(med_net_euc_list[:,np.newaxis])
    norm_med_net_cos = scaler.fit_transform(med_net_cos_list[:,np.newaxis])

    norm_sell_list = np.squeeze(np.asarray(norm_sell_list))
    norm_avg_hist_euc = 1 - np.squeeze(np.asarray(norm_avg_hist_euc))
    norm_avg_hist_cos = np.squeeze(np.asarray(norm_avg_hist_cos))
    norm_avg_net_euc = 1 - np.squeeze(np.asarray(norm_avg_net_euc))
    norm_avg_net_cos = np.squeeze(np.asarray(norm_avg_net_cos))
    norm_med_hist_euc = 1 - np.squeeze(np.asarray(norm_med_hist_euc))
    norm_med_hist_cos = np.squeeze(np.asarray(norm_med_hist_cos))
    norm_med_net_euc = 1 - np.squeeze(np.asarray(norm_med_net_euc))
    norm_med_net_cos = np.squeeze(np.asarray(norm_med_net_cos))
    
    # Compare the 8 signals (avg and median) with the sum of the sales
    # to get the 8 correlation values
    corr_mean_hist_cos = calculate_pearson(norm_avg_hist_euc, norm_sell_list)
    corr_mean_hist_euc = calculate_pearson(norm_avg_hist_cos, norm_sell_list)
    corr_mean_net_cos = calculate_pearson(norm_avg_net_euc, norm_sell_list)
    corr_mean_net_euc = calculate_pearson(norm_avg_net_cos, norm_sell_list)
    corr_median_hist_cos = calculate_pearson(norm_med_hist_euc, norm_sell_list)
    corr_median_hist_euc = calculate_pearson(norm_med_hist_cos, norm_sell_list)
    corr_median_net_cos = calculate_pearson(norm_med_net_euc, norm_sell_list)
    corr_median_net_euc = calculate_pearson(norm_med_net_cos, norm_sell_list)


    plt.figure(figsize=(10,4))
    plt.plot(norm_sell_list)
    plt.savefig("product_sell/sell_sum_norm")

    fig, axs = plt.subplots(4,1,figsize=(10,4))
    fig.tight_layout()
    fig.suptitle("Mean")
    axs[0].plot(norm_avg_hist_cos)
    axs[0].set_title("Histogram cosine")
    axs[1].plot(norm_avg_hist_euc)
    axs[1].set_title("Histogram euclidean")
    axs[2].plot(norm_avg_net_cos)
    axs[2].set_title("Classifier cosine")
    axs[3].plot(norm_avg_net_euc)
    axs[3].set_title("Classifier euclidean")
    fig.savefig("product_sell/mean_signal_norm")

    fig, axs = plt.subplots(4,1,figsize=(10,4))
    fig.tight_layout()
    fig.suptitle("Median")
    axs[0].plot(norm_med_hist_cos)
    axs[0].set_title("Histogram cosine")
    axs[1].plot(norm_med_hist_euc)
    axs[1].set_title("Histogram euclidean")
    axs[2].plot(norm_med_net_cos)
    axs[2].set_title("Classifier cosine")
    axs[3].plot(norm_med_net_euc)
    axs[3].set_title("Classifier euclidean")
    fig.savefig("product_sell/median_signal_norm")

plt.show()
conn.close()

# Save results on file
with open('final_values_norm.txt', 'w') as f:
    sys.stdout = f # Change the standard output to the file we created.
    print("corr_mean_hist_cos | corr_mean_hist_euc | corr_mean_net_cos | corr_mean_net_euc | corr_median_hist_cos | corr_median_hist_euc | corr_median_net_cos | corr_median_net_euc\n")
    print(corr_mean_hist_cos,corr_mean_hist_euc,corr_mean_net_cos,corr_mean_net_euc,corr_median_hist_cos,corr_median_hist_euc,corr_median_net_cos,corr_median_net_euc)

