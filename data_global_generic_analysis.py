import sys
import skimage.io
from scipy.spatial import distance
from matplotlib import pyplot as plt
import numpy as np
import datetime
import queue
from threading import Thread
import os
from dateutil.relativedelta import relativedelta
from sqlite3 import Error
from resnet import FeaturesExtractor
import cv2
import path_utils
from dateutil.relativedelta import relativedelta
import csv
from threading import Lock
from scipy.stats import linregress
from db_utils import create_db_connection


conn =  create_db_connection("database/database.db")
cur = conn.cursor()

create_view = """
    CREATE TEMP VIEW IF NOT EXISTS enum_distances AS 
        SELECT d3.*, enum.row_num
            FROM distances d3 JOIN (SELECT ROW_NUMBER () OVER (
                PARTITION BY d2.external_code, d2.color, d2.category
                ORDER BY d2.from_date ASC
            ) row_num, d2.external_code, d2.from_date
            FROM (
                SELECT d.*
                FROM distances d
                GROUP BY d.external_code, d.from_date
            ) d2) enum ON d3.external_code = enum.external_code AND d3.from_date = enum.from_date
"""

query_find = """
    SELECT ed.row_num,
        mu.avg_hist_cos,
        mu.avg_hist_euc,
        mu.avg_net_cos,
        mu.avg_net_euc,
        AVG((ed.hist_cos - mu.avg_hist_cos) * (ed.hist_cos - mu.avg_hist_cos)) AS var_hist_cos,
        AVG((ed.hist_euc - mu.avg_hist_euc) * (ed.hist_euc - mu.avg_hist_euc)) AS var_hist_euc,
        AVG((ed.net_cos - mu.avg_net_cos) * (ed.net_cos - mu.avg_net_cos)) AS var_net_cos,
        AVG((ed.net_euc - mu.avg_net_euc) * (ed.net_euc - mu.avg_net_euc)) AS var_net_euc
    FROM enum_distances AS ed JOIN (
        SELECT  ed2.row_num,
                AVG(ed2.hist_cos) AS avg_hist_cos, 
                AVG(ed2.hist_euc) AS avg_hist_euc,
                AVG(ed2.net_cos) AS avg_net_cos, 
                AVG(ed2.net_euc) AS avg_net_euc
        FROM enum_distances AS ed2
        GROUP BY ed2.row_num
    ) AS mu ON mu.row_num = ed.row_num
    GROUP BY ed.row_num
"""

#query_find = "SELECT external_code, category, color, from_date, to_date, row_num FROM enum_distances GROUP BY external_code, row_num ORDER BY external_code, from_date"

cur.execute(create_view)
cur.execute(query_find)
res = cur.fetchall()

reslen = len(res)

hist_cos_list = np.zeros(reslen)
hist_euc_list = np.zeros(reslen)
net_cos_list = np.zeros(reslen)
net_euc_list = np.zeros(reslen)
hist_cos_list2 = np.zeros(reslen)
hist_euc_list2 = np.zeros(reslen)
net_cos_list2 = np.zeros(reslen)
net_euc_list2 = np.zeros(reslen)
x_axis = range(0,reslen)

for idx, row in enumerate(res):
    print(row)

    hist_cos_list[idx] = row[1]
    hist_euc_list[idx] = row[2]
    net_cos_list[idx] = row[3]
    net_euc_list[idx] = row[4]

    hist_cos_list2 = row[5]
    hist_euc_list2 = row[6]
    net_cos_list2 = row[7]
    net_euc_list2 = row[8]

fig, axs = plt.subplots(2,2,figsize=(15,10))

axs[0,0].plot(x_axis, hist_cos_list)
axs[0,0].set_title("Histogram cosine")
#plt.plot(mu, c=colors[color])     
axs[0,0].fill_between(np.arange(0, reslen), hist_cos_list - hist_cos_list2 / 2, hist_cos_list + hist_cos_list2 / 2, alpha=0.3, fc='tab:orange')

axs[0,1].plot(x_axis, hist_euc_list)
axs[0,1].set_title("Histogram euclidean")
#plt.plot(mu, c=colors[color])     
axs[0,1].fill_between(np.arange(0, reslen), hist_euc_list - hist_euc_list2 / 2, hist_euc_list + hist_euc_list2 / 2, alpha=0.3, fc='tab:green')

axs[1,0].plot(x_axis, net_cos_list)
axs[1,0].set_title("Net cosine")
#plt.plot(mu, c=colors[color])     
axs[1,0].fill_between(np.arange(0, reslen), net_cos_list - net_cos_list2 / 2, net_cos_list + net_cos_list2 / 2, alpha=0.3, fc='tab:red')

axs[1,1].plot(x_axis, net_euc_list)
axs[1,1].set_title("Net euclidean")
#plt.plot(mu, c=colors[color])     
axs[1,1].fill_between(np.arange(0, reslen), net_euc_list - net_euc_list2 / 2, net_euc_list + net_euc_list2 / 2, alpha=0.3, fc='tab:blue')


plt.show()
conn.close()