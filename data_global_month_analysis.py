import sys
import skimage.io
from scipy.spatial import distance
from matplotlib import pyplot as plt
import numpy as np
import datetime
import queue
from threading import Thread
import os
from dateutil.relativedelta import relativedelta
from sqlite3 import Error
from resnet import FeaturesExtractor
import cv2
import path_utils
from dateutil.relativedelta import relativedelta
import csv
from threading import Lock
from scipy.stats import linregress
from db_utils import create_db_connection


conn =  create_db_connection("database/database.db")
cur = conn.cursor()

query_find_per_month_4_years = """SELECT strftime('%m',d.from_date) AS month,
                                mu.avg_hist_cos,
                                mu.avg_hist_euc,
                                mu.avg_net_cos,
                                mu.avg_net_euc,
                                AVG((d.hist_cos - mu.avg_hist_cos) * (d.hist_cos - mu.avg_hist_cos)) AS var_hist_cos,
                                AVG((d.hist_euc - mu.avg_hist_euc) * (d.hist_euc - mu.avg_hist_euc)) AS var_hist_euc,
                                AVG((d.net_cos - mu.avg_net_cos) * (d.net_cos - mu.avg_net_cos)) AS var_net_cos,
                                AVG((d.net_euc - mu.avg_net_euc) * (d.net_euc - mu.avg_net_euc)) AS var_net_euc
                            FROM distances AS d JOIN (
                                SELECT strftime('%m',d2.from_date) as avg_month,
                                       AVG(d2.hist_cos) AS avg_hist_cos, 
                                       AVG(d2.hist_euc) AS avg_hist_euc,
                                       AVG(d2.net_cos) AS avg_net_cos, 
                                       AVG(d2.net_euc) AS avg_net_euc
                                FROM distances AS d2
                                GROUP BY strftime('%m',d2.from_date)
                            ) AS mu ON mu.avg_month = strftime('%m',d.from_date)
                            GROUP BY strftime('%m', d.from_date)"""

cur.execute(query_find_per_month_4_years)
res = cur.fetchall()

reslen = len(res)

hist_cos_list = np.zeros(reslen)
hist_euc_list = np.zeros(reslen)
net_cos_list = np.zeros(reslen)
net_euc_list = np.zeros(reslen)
hist_cos_list2 = np.zeros(reslen)
hist_euc_list2 = np.zeros(reslen)
net_cos_list2 = np.zeros(reslen)
net_euc_list2 = np.zeros(reslen)
x_axis = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

for idx, row in enumerate(res):
    print(row)

    hist_cos_list[idx] = row[1]
    hist_euc_list[idx] = row[2]
    net_cos_list[idx] = row[3]
    net_euc_list[idx] = row[4]

    hist_cos_list2 = row[5]
    hist_euc_list2 = row[6]
    net_cos_list2 = row[7]
    net_euc_list2 = row[8]

fig, axs = plt.subplots(2,2,figsize=(15,10))
fig.autofmt_xdate()

axs[0,0].plot(x_axis, hist_cos_list)
axs[0,0].set_title("Histogram cosine")
#plt.plot(mu, c=colors[color])     
axs[0,0].fill_between(np.arange(0, reslen), hist_cos_list - hist_cos_list2 / 2, hist_cos_list + hist_cos_list2 / 2, alpha=0.3, fc='tab:orange')

axs[0,1].plot(x_axis, hist_euc_list)
axs[0,1].set_title("Histogram euclidean")
#plt.plot(mu, c=colors[color])     
axs[0,1].fill_between(np.arange(0, reslen), hist_euc_list - hist_euc_list2 / 2, hist_euc_list + hist_euc_list2 / 2, alpha=0.3, fc='tab:green')

axs[1,0].plot(x_axis, net_cos_list)
axs[1,0].set_title("Net cosine")
#plt.plot(mu, c=colors[color])     
axs[1,0].fill_between(np.arange(0, reslen), net_cos_list - net_cos_list2 / 2, net_cos_list + net_cos_list2 / 2, alpha=0.3, fc='tab:red')

axs[1,1].plot(x_axis, net_euc_list)
axs[1,1].set_title("Net euclidean")
#plt.plot(mu, c=colors[color])     
axs[1,1].fill_between(np.arange(0, reslen), net_euc_list - net_euc_list2 / 2, net_euc_list + net_euc_list2 / 2, alpha=0.3, fc='tab:blue')


plt.show()
conn.close()
