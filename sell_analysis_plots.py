from scipy import stats
from db_utils import create_db_connection
import csv
import numpy as np
import sys
import matplotlib.pyplot as plt

conn =  create_db_connection("database/database.db")
cur = conn.cursor()

#get_mean_values("AM1377CC179FA-17")
# Read the dataset
with open('product_sell/sell_dataset.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')

    sell_list = []
    x_list = []

    for n, row in enumerate(csv_reader):
        if(n>0):
            # Get the external code (id of product)
            ext_code = row[0]
            name = row[14]

            print(ext_code, name)

            if("dress" in name.lower()):
                # Extract the 12 months sell data and sum it
                sell_data = np.array(row[1:13])
                sell_data = sell_data.astype(np.float)
                # Append it to the list to create the signal
                sell_list.append(np.sum(sell_data))
                x_list.append(ext_code)


plt.figure(figsize=(10,4))
plt.plot(sell_list)
plt.savefig("product_sell/sell_sum")
plt.show()



conn.close()
